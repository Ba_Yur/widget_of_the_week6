import 'package:flutter/material.dart';
import './page.dart';
import 'initial_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      home: MyHomePage(),
    );
  }
}


class MyHomePage extends StatefulWidget {




  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  // final controller = PageController(
  //   initialPage: 1,
  // );

 final pageView = PageView(
      controller: PageController(
        initialPage: 0,
      ),
        children: [
          InitialPage(),
          NewPage(pageName: 'First page',pageColor: Colors.grey),
          NewPage(pageName: 'Second page',pageColor: Colors.green),
          NewPage(pageName: 'Third page',pageColor: Colors.blue),
        ],
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pageView,
    );
  }
}
