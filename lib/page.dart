
import 'package:flutter/material.dart';

class NewPage extends StatelessWidget {

  final String pageName;
  final Color pageColor;

  const NewPage({this.pageName, this.pageColor});




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: pageColor,
      appBar: AppBar(
        title: Text(pageName),
      ),body: Center(
      child: Text(pageName, style: TextStyle(
        fontSize: 50
      ),),
    ),
    );
  }
}
