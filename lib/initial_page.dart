import 'package:flutter/material.dart';
import './large_img.dart';

class InitialPage extends StatefulWidget {
  const InitialPage({Key key}) : super(key: key);

  @override
  _InitialPageState createState() => _InitialPageState();
}


class _InitialPageState extends State<InitialPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Initial page'),
      ),
      body: Center(
        child: GestureDetector(
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (_) {
              return LargeImg();
            }));
          },
          child: AspectRatio(
            aspectRatio: 2/1,
            child: Hero(
                tag: 'img',
                child: ColorFiltered(
                  colorFilter: ColorFilter.mode(Colors.grey, BlendMode.color),
                  child: Image.network(
                    'https://img3.akspic.ru/originals/2/8/8/7/4/147882-poni-rozovyj-liniya-kartinka-televideniye-750x1334.jpg',
                    fit: BoxFit.cover,
                  ),
                )),
          ),
        ),
      ),
    );
  }
}
