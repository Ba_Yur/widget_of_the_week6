

import 'package:flutter/material.dart';

class LargeImg extends StatelessWidget {
  const LargeImg({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      width: 300,
      child: Hero(
          tag: 'img',
          child: Image.network(
            'https://img3.akspic.ru/originals/2/8/8/7/4/147882-poni-rozovyj-liniya-kartinka-televideniye-750x1334.jpg',
            fit: BoxFit.cover,
          )),
    );
  }
}
